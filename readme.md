# Beamer template from scratch

---

## Configuration

Latex image from dockerhub
> docker pull schickling/latex

## Run

Up latex container mapping your _pwd_ to _/source_ into the container

> docker run --rm -it -v $(pwd):/source schickling/latex

Generate your `'.pdf'`

> pdflatex [YOUR_FILE].tex
